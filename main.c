#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.h"
#include "helper_functions.h"
#include "wav_types.h"

int main(int argc, char *argv[])
{
    FILE *inputFile;
    char *inputFileRoute;

    get_commandline_args(&inputFileRoute, argc, argv);

    // We check if there isn't available file route from the command line. If there isn't presented we have to enter one.
    if(!inputFileRoute){
      printf("Enter file route: ");
      scanf("%s", &inputFileRoute);
    }

    // Check the file string length and verify its extension
    int fileLength = strlen(inputFileRoute);
    bool isValidFile = verify_file_extension(inputFileRoute, fileLength);
    if(!isValidFile){
      printf("This file format is not supported!");
      return 0;
    }

    //Check if there is real file presented.
    inputFile = fopen(inputFileRoute,"rb");
    if (inputFile == NULL) {
      printf("Failed to open the file. Wrong path!\nFile path: %s", inputFileRoute);
      return 0;
    }
    
    //Checking the file size of the presented file. We need to go to the end of the file and then return to its start and allocate much memory as needed.
    fseek(inputFile, 0L, SEEK_END);
    int fileSize = ftell(inputFile);
    fseek(inputFile, 0L, SEEK_SET);
    char *fileData = malloc(fileSize);
    fread(fileData, 1, fileSize, inputFile);
    
    struct header_struct *file=(struct header_struct*)fileData;
    
    get_channels(file);
    get_file_parsed_size(fileSize);
    char *chosenChannel = mute_file(file);
    write_wav(fileLength, inputFileRoute, chosenChannel, fileData, fileSize);
}
