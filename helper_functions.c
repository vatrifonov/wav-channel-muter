#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool verify_file_extension(char inputFileRoute[], int fileLength){
    return inputFileRoute[fileLength-4] == '.' && inputFileRoute[fileLength-3] == 'w' && inputFileRoute[fileLength-2] == 'a' && inputFileRoute[fileLength-1] == 'v';
}
