#ifndef FUNCTIONS_H
#define FUNCTIONS_H

char* mute_file(struct header_struct* file);
void mute(struct header_struct* file, char channel);
void write_wav(int fileLength, char *inputFileRoute, char *channel, struct header_struct *fileData, int fileSize);
void get_file_parsed_size(int sizeBytes);
void get_channels(struct header_struct *file);
void get_commandline_args(char **file,int argc, char *argv[]);

#endif