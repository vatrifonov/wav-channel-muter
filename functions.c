
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include "wav_types.h"

static int s_muted=0,s_unit=0;
static bool programIsMuted = false, unitChanged = false;
static char unit='b';
static char choice[1000];
const int mbInBytes = 1048576;
const int kbInBytes = 1024;


//We use this function to mute the preferred channel in the audio file
char* mute_file(struct header_struct* file, short *data){
   //Check if the file has 2 channels. Otherwise it should throw an error.
   if((*file).num_channels.short_value == 2){
      char choice[50];
      while(strcmp("right", choice) != 0 || strcmp("left", choice) != 0){
         if(!programIsMuted) printf("Enter channel to mute (left/right): ");
         scanf("%s", &choice);
         if(strcmp("right", choice) == 0){
            mute(file, 'r');
            return 'r';
         } else if (strcmp("left", choice) == 0) {
            mute(file, 'l');
            return 'l';
         }
      }
   } else {
      if(!programIsMuted){
         printf("This file has only 1 channel and it\'s not available for muting. \n");
      }
      return 0;
   }
}

// Mute the file
void mute(struct header_struct* file, char channel){
   int channel_index = channel == 'l' ? 0 : 1;
   int num_samples_per_channel = (*file).subchunk2_size.int_value / sizeof(short);

   // We are using this to get a reference to all the samples in the selected audio file
   short *samples=(short*)(file + HEADER_SIZE) + channel_index;

   int mutedSamples = 0;
   for (int i = 0; i < num_samples_per_channel; i+=2){
      if(samples[i] != 0){
         mutedSamples++;
         samples[i] = 0;
      }
   }
   if(!programIsMuted){
      printf("%d samples on the %s were replaced with 0\n", mutedSamples, channel == 'l' ? "left" : "right");
   }
}

// Write the edited audio file as new a file with new name in the format [name]-[selected channel]-channel-muted.wav. It saves in the root folder of the program.
void write_wav(int fileLength, char *inputFileRoute, char *channel, struct header_struct *fileData, int fileSize){
   char newFileName[1000];
    for(int i = 0; i < fileLength; i++){
      newFileName[i] = inputFileRoute[i];
    }

    for(int i = fileLength ; i>0; i--){
      if(newFileName[i] == '.'){
        break;
      }
    }
    char newFileRoute[500];
    snprintf(newFileRoute, sizeof(newFileRoute), "%s-%s-channel-muted.wav", newFileName, channel == 'l' ? "left" : "right");
    FILE* newFile=fopen(newFileRoute,"wb");
    if (newFile == NULL) {
      printf("Failed to open file: %s\n", newFileRoute);
    }
    fwrite(fileData,sizeof(char),fileSize,newFile);
}

// Get the size in bytes and decide which value to choose depending of the input unit. If not selected then we transform the size as usual.
void get_file_parsed_size(int sizeBytes){
   double size = (double)sizeBytes;
   if(unitChanged){
    switch(unit){
      case 'b':
         break;
      case 'k':
         size /= (double)kbInBytes;
         break;
      case 'm':
         size /= (double)mbInBytes;
         break;
    }
   } else {
    if(size < kbInBytes){
        unit = 'b';
    } else if (size >= kbInBytes && size < mbInBytes){
        size /= (double)kbInBytes;
        unit = 'k';
    } else {
        size /= (double)mbInBytes;
        unit = 'm';
    }
   }

   if(!programIsMuted){
    printf("The sound content is %g ", size);
    switch(unit){
        case 'b':
            printf("B\n");
            break;
        case 'k':
            printf("KB\n");
            break;
        case 'm':
            printf("MB\n");
            break;
    }
   }

}

// Get the number of channels.
void get_channels(struct header_struct *file){
   if(!programIsMuted) printf("Number of channels: %d \n", (*file).num_channels);
}

// When we start the program from the command line we have an opiton to add agruments. This function checks these values.
void get_commandline_args(char **file,int argc, char *argv[]){
   if(argc > 1){
      for(int i = 1; i < argc; i++){
         if(strlen(argv[i]) == 2){
            if(strcmp("-s",argv[i]) == 0){
               programIsMuted = true;
            } else if(strcmp("-f",argv[i]) == 0){
               *file = argv[i+1];
               i++;
            } else if(strcmp("-u",argv[i]) == 0){
               switch(tolower(argv[i+1][0])){
                case 'b':
                    unit = 'b';
                    unitChanged = true;
                    break;
                case 'k':
                    unit = 'k';
                    unitChanged = true;
                    break;
                case 'm':
                    unit = 'm';
                    unitChanged = true;
                    break;
               }
               i++;
            }
         }
      }
   }
}





